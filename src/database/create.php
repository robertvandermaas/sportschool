<?php

// Function createSport inserts a new sport into the database
function createSport($db, $name, $amount) {
    try {
        $q = $db->prepare("INSERT INTO sporten (sportnaam, sportbedrag) VALUES (:sportName, :amount)");
        $q->bindParam("sportName", $name);
        $q->bindParam("amount", $amount);
        if ($q->execute()) {
            echo "Sport " . $name . " is toegevoegd aan de database\n";
        } else {
            echo "Kon sport " . $name . " niet aan database toevoegen\n";
        } 
    } catch (PDOException $e) {
        die("Could not insert sport into database: " . $e->getMessage());
    }
}

// Function createTrainer inserts a new trainer into the database
function createTrainer($db, $name, $gender, $telNumber, $email) {
    try {
        $q = $db->prepare("INSERT INTO begeleiders (naam_begeleider, geslacht_begeleider, telefoonnummer_begeleider, email_begeleider)
                            VALUES (:trainerName, :gender, :telNumber, :email)");
        $q->bindParam("trainerName", $name);
        $q->bindParam("gender", $gender);
        $q->bindParam("telNumber", $telNumber);
        $q->bindParam("email", $email);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not insert trainer into database: " . $e->getMessage());
    } 
}

// Function createSportTrainer links a sport to a trainer
function createSportTrainer($db, $trainerID, $sportID, $sportYear, $info) {
    try {
        $q = $db->prepare("INSERT INTO sportbegeleider (ID_begeleider, sportcode, sportjaar, bijzonderheden) 
                            VALUES (:trainerID, :sportID, :sportYear, :info)");
        $q->bindParam("trainerID", $trainerID);
        $q->bindParam("sportID", $sportID);
        $q->bindParam("sportYear", $sportYear);
        $q->bindParam("info", $info);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not create a sport trainer: " . $e->getMessage());
    }
}

// Function createPractisedSport links a member to a sport
function createPractisedSport($db, $memberID, $sportCode, $contributionYear, $contribution, $payment) {
    try {
        $q = $db->prepare("INSERT INTO beoefende_sporten (ID_lid, sportcode, contributiejaar, contributiebedrag, betaald)
                            VALUES (:memberID, :sportCode, :contributionYear, :contribution, :payment)");
        $q->bindParam("memberID", $memberID);
        $q->bindParam("sportCode", $sportCode);
        $q->bindParam("contributionYear", $contributionYear);
        $q->bindParam("contribution", $contribution);
        $q->bindParam("payment", $payment);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not create a practised sport: " . $e->getMessage());
    }
}

// Function createMember inserts a new member into the database
function createMember($db, $first_name, $last_name, $gender, $postalcode, $city, $tel, $mail) {
    try {
        $q = $db->prepare("INSERT INTO leden (voornaam_lid, achternaam_lid, geslacht_lid, postcode_lid, woonplaats_lid, telefoonnummer_lid, emailadres_lid)
                            VALUES (:first_name, :last_name, :gender, :postalcode, :city,  :tel, :mail)");
        $q->bindParam("first_name", $first_name);
        $q->bindParam("last_name", $last_name);
        $q->bindParam("gender", $gender);
        $q->bindParam("postalcode", $postalcode);
        $q->bindParam("city", $city);
        $q->bindParam("tel", $tel);
        $q->bindParam("mail", $mail);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not insert member into database: " . $e->getMessage());
    } 
}