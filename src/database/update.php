<?php

// Function updateTrainer updates the information about a trainer.
function updateTrainer($db, $id, $name, $gender, $tel, $mail) {
    try {
        $q = $db->prepare("UPDATE `begeleiders` SET naam_begeleider=:name, geslacht_begeleider=:gender,
                         telefoonnummer_begeleider=:tel, email_begeleider=:mail
                         WHERE ID_begeleider=:id");
        $q->bindParam("name", $name);
        $q->bindParam("gender", $gender);
        $q->bindParam("tel", $tel);
        $q->bindParam("mail", $mail);
        $q->bindParam("id", $id);
        if ($q->execute()) {
            echo "Opgeslagen.\n";
        } else {
            echo "Kon gegevens niet opslaan.\n";
        }
    } catch (PDOException $e) {
        die("Could not save changes of trainer: " . $e->getMessage());
    }
}

function updateMember($db, $id, $firstName, $lastName, $gender, $postalCode, $city, $tel, $mail) {
    try {
        $q = $db->prepare("UPDATE `leden` SET voornaam_lid=:firstName, achternaam_lid=:lastName, geslacht_lid=:gender,
                         postcode_lid=:postalCode, woonplaats_lid=:city, telefoonnummer_lid=:tel, emailadres_lid=:mail
                         WHERE ID_lid=:id");
        $q->bindParam("firstName", $firstName);
        $q->bindParam("lastName", $lastName);
        $q->bindParam("gender", $gender);
        $q->bindParam("postalCode", $postalCode);
        $q->bindParam("city", $city);
        $q->bindParam("tel", $tel);
        $q->bindParam("mail", $mail);
        $q->bindParam("id", $id);
        if ($q->execute()) {
            echo "Opgeslagen.\n";
        } else {
            echo "Kon gegevens niet opslaan.\n";
        }
    } catch (PDOException $e) {
        die("Could not save changes of member: " . $e->getMessage());
    }
}

// Function updateSport updates the information about a sport.
function updateSport($db, $id, $sport, $amount) {
    try {
        $q = $db->prepare("UPDATE `sporten` SET sportnaam=:sport, sportbedrag=:amount WHERE sportcode=:id");
        $q->bindParam("sport", $sport);
        $q->bindParam("amount", $amount);
        $q->bindParam("id", $id);
        if ($q->execute()) {
            echo "Opgeslagen.\n";
        } else {
            echo "Kon gegevens niet opslaan.\n";
        }
    } catch (PDOException $e) {
        die("Could not save changes of sport: " . $e->getMessage());
    }
}

// Function updatePayment updates the payment status of a practised sport.
function updatePayment($db, $memberID, $sportCode, $contributionYear, $payment) {
    try {
        $q = $db->prepare("UPDATE `beoefende_sporten` SET betaald=:payment WHERE (ID_lid, sportcode, contributiejaar) = (:memberID, :sportCode, :contributionYear)");
        $q->bindParam("payment", $payment);
        $q->bindParam("memberID", $memberID);
        $q->bindParam("sportCode", $sportCode);
        $q->bindParam("contributionYear", $contributionYear);
        if ($q->execute()) {
            echo "Opgeslagen.\n";
        } else {
            echo "Kon gegevens niet opslaan.\n";
        }
    } catch (PDOException $e) {
        die("Could not save changes of practised sport: " . $e->getMessage());
    }
}