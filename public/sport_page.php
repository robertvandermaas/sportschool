<!DOCTYPE HTML>

<head>
    <title>Sporten</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/sport_page.css"/>
    <link rel="stylesheet" type="text/css" href="styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="styles/form.css"/>
</head>

<body>
<div class="wrapper">
    <div class="heading">
        <header>
            <h1>Sporten</h1>
        </header>
    </div>
    <div  class="sidebar">
        <div class="sidebar-sports">
            <?php include 'sidebar.php'; ?>
        </div>
    </div>
    <div class="content">
        <div class="content-padding">
            <div class="table-parent">
                <table>
                    <?php
                    include '../src/database/get.php';
                    include '../src/database/delete.php';
                    include '../src/database/update.php';
                    include '../src/database/database.php';
                    include '../src/database/create.php';

                    date_default_timezone_set("Europe/Amsterdam");
                    $db = connect();
                    $sports = getSports($db);
                    ?>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Sport</th>
                        <th>Contributie</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sports = getSports($db);
                    foreach ($sports as $sport) {
                        echo "<tr class='$sport[sportcode]'>";
                        foreach ($sport as $sportData) {
                            echo "<td>" . $sportData . "</td>";
                        }
                        echo "<td><form action='' method='post'>
                        <button id='edit-but' type='submit' name='edit' value='$sport[sportcode]'>Bewerk</button>
                        </form></td>";
                        echo "</tr>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?php
                if (isset($_POST['edit'])) {
                    $sportcode = $_POST['edit'];
                    $sport = getSport($db, $sportcode);
                    $name = $sport[0]['sportnaam'];
                    $amount = $sport[0]['sportbedrag'];

                    echo "<div class='overlay'>";
                        echo "<div class='overlay-content'>";
                            echo "<div class='flex-row'>";
                                echo "<div class='edit-form'>";
                                    echo "<form class='edit-form' action='' method='post'>";
                                        echo "<label for='sport'>Sport</label>";
                                        echo "<input id='sport' name='sport' type='text' value='$name'
                                                placeholder='Sportnaam' required/><br><br>";
                                        echo "<label for='amount'>Bedrag</label>";
                                        echo "<input id='amount' type='number' name='amount' value='$amount'
                                                min='1' max='999.99' placeholder='999.99' step='0.01' required/><br><br><br>";
                                        echo "<button id='save-but' type='submit' name='save' value='$sportcode'>Opslaan</button>";
                                        echo "<input id='cancel-but' type='button' name='cancel' value='Terug'
                                                onclick='location.href=`sport_page.php`'/>";
                                        echo "<button id='del-but' type='submit' name='delete' value='$sportcode'>Verwijderen</button>";
                                    echo "</form>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                }
                echo "<div class='resp'>";

                    function validateEditedSport($sport, $amount): bool {
                        $filter_opts = array('options' => array('min_range' => 1, 'max_range' => 999.99));
                        if (($sport != "") &&
                            ($amount != "") &&
                            (filter_var($amount, FILTER_VALIDATE_FLOAT, $filter_opts))) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                    if (isset($_POST['delete'])) {
                        $sportcode = $_POST['delete'];
                        deleteSport($db, $sportcode);
                        header("Refresh:0");
                    }

                    if (isset($_POST['save'])) {
                        $sportcode = $_POST['save'];
                        $sport = $_POST['sport'];
                        $amount = $_POST['amount'];
                        if (validateEditedSport($sport, $amount)) {
                            updateSport($db, $sportcode, $sport, $amount);
                            header("Refresh:0");
                        } else {
                            echo "Invoer niet geldig. Probeer opnieuw.\n";
                        }
                    }
                echo "</div>";
            ?>
            <div class="button-new">
            <button id="new-but" onclick="location.href = 'new_sport.php'">Nieuw</button>
        </div>
    </div>
</body>