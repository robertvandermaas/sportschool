<!DOCTYPE html>

<head>
    <title>Leden</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/member_page.css"/>
    <link rel="stylesheet" type="text/css" href="styles/form.css"/>
    <link rel="stylesheet" type="text/css" href="styles/table.css"/>
    
</head>

<body>
    <div class="wrapper">
        <div class="heading">
            <header>
             <h1>Leden</h1>
            </header>
        </div>
    
        <div class="sidebar">
            <div class="sidebar-members">
                <?php include 'sidebar.php'; ?>
            </div>
        </div>
        <div class="content">
            <div class="content-padding">
                <div class="table-parent">
                    <table>
                        <thead>
                            <tr>
                                <th class="id">ID</th>
                                <th class="fname">Voornaam</th>
                                <th class="lname">Achternaam</th>
                                <th class="gender">Geslacht</th>
                                <th class="postalcode">Postcode</th>
                                <th class="city">Woonplaats</th>
                                <th class="phone">Telefoonnummer</th>
                                <th class="email">E-mail</th>
                                <th class="del-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include '../src/database/get.php';
                            include '../src/database/delete.php';
                            include '../src/database/update.php';
                            include '../src/database/database.php';
                            include '../src/database/create.php';

                            date_default_timezone_set("Europe/Amsterdam");
                            $db = connect();
                            $members = getMembers($db);
                            foreach ($members as $member) {
                                echo "<tr>";
                                foreach ($member as $memberData) {
                                    echo "<td>" . $memberData . "</td>";
                                }
                                echo "<td class='del-col'><form class='del-form' action='' method='post'>
                                    <button id='edit-but' type='submit' name='edit' value='$member[ID_lid]'>Bewerk</button>
                                    </form></td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php
                if (isset($_POST['edit'])) {
                    $id = $_POST['edit'];
                    $member = getMember($db, $id);
                    $firstName = $member[0]['voornaam_lid'];
                    $lastName = $member[0]['achternaam_lid'];
                    $gender = $member[0]['geslacht_lid'];
                    $postalCode = $member[0]['postcode_lid'];
                    $city = htmlspecialchars($member[0]['woonplaats_lid'], ENT_QUOTES);
                    $tel = $member[0]['telefoonnummer_lid'];
                    $mail = $member[0]['emailadres_lid'];
                    echo "<div class='overlay'>";
                        echo "<div class='overlay-content'>";
                            echo "<div class='flex-row'>";
                                echo "<div class='edit-form'>";
                                    echo "<form class='edit-form' action='' method='post'>";
                                        echo "<label for='first-name'>Voornaam</label>";
                                        echo "<input id='first-name' type='text' name='firstName' value='$firstName' placeholder='Voornaam' required/><br><br>";
                                        echo "<label for='last-name'>Achternaam</label>";
                                        echo "<input id='last-name' type='text' name='lastName' value='$lastName' placeholder='Achternaam' required/><br><br>";
                                        echo "<label for='gender-input'>Geslacht</label>";
                                        if ($gender == 'm') {
                                            echo "<input id='gender-input' type='radio' name='gender' value='m' checked required/>m";
                                            echo "<input id='gender-input' type='radio' name='gender' value='v' required/>v<br><br>";
                                        } else {
                                            echo "<input id='gender-input' type='radio' name='gender' value='m' required/>m";
                                            echo "<input id='gender-input' type='radio' name='gender' value='v' checked required/>v<br><br>";
                                        }
                                        echo "<label for='postal-code'>Postcode</label>";
                                        echo "<input id='postal-code' type='text' name='postalCode' value='$postalCode' placeholder='0000AA' pattern='[0-9]{4}[A-Z]{2}' required/><br><br>";
                                        echo "<label for='city'>Woonplaats</label>";
                                        echo "<input id='city' type='text' name='city' value=$city placeholder='Woonplaats' required/><br><br>";
                                        echo "<label for='tel'>Telefoonnummer</label>";
                                        echo "<input id='tel' type='tel' name='tel' pattern='[0-9]{10}' value='$tel' placeholder='0612345678' required/><br><br>";
                                        echo "<label for='mail'>Email</label>";
                                        echo "<input id='mail' type='email' name='mail' value='$mail' placeholder='example@example.com' required/><br><br><br>";
                                        echo "<button id='save-but' type='submit' value='$id' name='save'>Opslaan</button>";
                                        echo "<input id='cancel-but' type='button' value='Terug' onclick='location.href=`members_page.php`'/>";
                                        echo "<button id='del-but' name='delete' value='$id' type='submit'>Verwijderen</button>";
                                    echo "</form>";
                                echo "</div>";
                                echo "<div class='new-practised-sport-form'>";
                                    echo "<form class='new-practised-sport-form' action='' method='post'>";
                                        echo "<label for='sport'>Sport</label>";
                                        echo "<select id='sport' name='sport'>";
                                            $sports = getSports($db);
                                            foreach ($sports as $sport) {
                                                echo "<option value='$sport[sportcode]'>$sport[sportnaam] - €$sport[sportbedrag]</option>";
                                            }
                                        echo "</select><br><br>";
                                        $currentYear = date("o");
                                        $maxYear = $currentYear + 2;
                                        echo "<label for='contribution-year'>Contributiejaar</label>";
                                        echo "<input id='contribution-year' name='contributionYear' type='number' placeholder='$currentYear'
                                        min='$currentYear' max='$maxYear' required/><br><br>";
                                        echo "<label for='payment-status-input'>Betaald</label>";
                                        echo "<input id='payment-status-input' type='radio' name='paymentStatus' value='ja' required/>Ja";
                                        echo "<input id='payment-status-input' type='radio' name='paymentStatus' value='nee' required/>Nee<br><br><br>";
                                        echo "<button id='save-but' type='submit' name='savePractisedSport' value='$id'>Opslaan</button>";
                                        echo "<input id='cancel-but' type='button' value='Terug' onclick='location.href=`members_page.php`'/>";
                                    echo "</form>";
                                echo "</div>";
                            echo "</div>";
                            echo "<div class='practised-sport-table'>";
                                echo "<table>";
                                    echo "<thead>";
                                        echo "<tr>";
                                            echo "<th>Sportcode</th>";
                                            echo "<th>Sport</th>";
                                            echo "<th>Contributiejaar</th>";
                                            echo "<th>Bedrag</th>";
                                            echo "<th>Betaald</th>";
                                            echo "<th></th>";
                                        echo "</tr>";
                                    echo "</thead>";
                                    echo "<tbody>";
                                        $practisedSports = getPractisedSports($db, $id);
                                        foreach ($practisedSports as $practisedSport) {
                                            $sportCode = $practisedSport['sportcode'];
                                            $sport = $practisedSport['sportnaam'];
                                            $contributionYear = $practisedSport['contributiejaar'];
                                            $contribution = $practisedSport['contributiebedrag'];
                                            $payment = $practisedSport['betaald'];
                                            echo "<tr>";
                                                echo "<td>$sportCode</td>";
                                                echo "<td>$sport</td>";
                                                echo "<td>$contributionYear</td>";
                                                echo "<td>$contribution</td>";
                                                $unique = "$id-$sportCode-$contributionYear";
                                                if ($payment == 'ja') {
                                                    echo "<td><form action='' method='post'>
                                                            <select id='payment' name='payment'>
                                                                <option value='ja' selected>ja</option>
                                                                <option value='nee'>nee</option>
                                                            </select>
                                                            <button id='edit-but' type='submit'
                                                            value='$unique' name='savePayment'>Opslaan</button>
                                                        </form></td>";
                                                } else {
                                                    echo "<td><form action='' method='post'>
                                                            <select id='payment' name='payment'>
                                                                <option value='ja'>ja</option>
                                                                <option value='nee' selected>nee</option>
                                                            </select>
                                                            <button id='edit-but' type='submit'
                                                            value='$unique' name='savePayment'>Opslaan</button>
                                                        </form></td>";
                                                }
                                                echo "<td><form action='' method='post'><button id='edit-but' type='submit'
                                                value='$unique' name='delPractisedSportBut'>Verwijder</button></form></td>";
                                            echo "</tr>";
                                        }
                                    echo "</tbody>";
                                echo "</table>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                }
                echo "<div class='resp'>";

                function validateMemberID($db, $memberID): bool {
                    $members = getMembers($db);

                    foreach ($members as $member) {
                        if ($member['ID_lid'] == $memberID) {
                            return true;
                        }
                    }
                    return false;
                }

                function validateSportCode($db, $sportCode): bool {
                    $sports = getSports($db);

                    foreach ($sports as $sport) {
                        if ($sport['sportcode'] == $sportCode) {
                            return true;
                        }
                    }
                    return false;
                }

                function validateMember($first_name, $last_name, $gender, $postalCode, $city, $tel, $mail): bool {
                    if (($first_name != "") &&
                        ($last_name != "") &&
                        ($gender != "") &&
                        ($gender == 'm' || $gender == 'v') &&
                        ($postalCode != "") &&
                        (filter_var($postalCode, FILTER_VALIDATE_REGEXP,  array("options"=>array("regexp"=>"/[0-9]{4}[A-Z]{2}/")))) &&
                        ($city != "") &&
                        ($tel != "") &&
                        (is_numeric($tel)) &&
                        (strlen($tel) == 10) &&
                        ($mail != "") &&
                        (filter_var($mail, FILTER_VALIDATE_EMAIL))) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function validatePracticedSport($contributionYear, $paymentStatus): bool {
                    $currentYear = date("o");
                    $maxYear = $currentYear + 2;
                    if (($contributionYear != "") &&
                        ($contributionYear >= $currentYear && $contributionYear <= $maxYear) &&
                        ($paymentStatus != "") &&
                        ($paymentStatus == 'ja' || $paymentStatus == 'nee' )) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function validateUniquePractisedSport($db, $memberID, $sportCode, $contributionYear): bool {
                    $practisedSports = getPractisedSports($db, $memberID);
                    foreach ($practisedSports as $practisedSport) {
                        if ($practisedSport['sportcode'] == $sportCode && $practisedSport['contributiejaar'] == $contributionYear) {
                            return false;
                        }
                    }
                    return true;
                }

                if (isset($_POST['delete'])) {
                    $id = $_POST['delete'];
                    if (validateMemberID($db, $id)) {
                        deleteMember($db, $id);
                        echo "<META HTTP-EQUIV=Refresh CONTENT='0'>";
                    } else {
                        echo "ID van te verwijderen lid niet geldig. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['save'])) {
                    $id = $_POST['save'];
                    $firstName = $_POST['firstName'];
                    $lastName = $_POST['lastName'];
                    $gender = $_POST['gender'];
                    $postalCode = $_POST['postalCode'];
                    $city = $_POST['city'];
                    $tel = $_POST['tel'];
                    $mail = $_POST['mail'];
                    if (validateMember($firstName, $lastName, $gender, $postalCode, $city, $tel, $mail) && validateMemberID($db, $id)) {
                        updateMember($db, $id, $firstName, $lastName, $gender, $postalCode, $city, $tel, $mail);
                        echo "<META HTTP-EQUIV=Refresh CONTENT='0'>";
                    } else {
                        echo "Invoer niet geldig. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['savePractisedSport'])) {
                    $memberID = $_POST['savePractisedSport'];
                    $sportCode = $_POST['sport'];
                    if (validateMemberID($db, $memberID) && validateSportCode($db, $sportCode)) {
                        $contributionYear = $_POST['contributionYear'];
                        $sport = getSport($db, $sportCode);
                        $contribution = $sport[0]['sportbedrag'];
                        $payment = $_POST['paymentStatus'];
                        if (validatePracticedSport($contributionYear, $payment)) {
                            if (validateUniquePractisedSport($db, $memberID, $sportCode, $contributionYear)) {
                                createPractisedSport($db, $memberID, $sportCode, $contributionYear, $contribution, $payment);
                            } else {
                                echo "Gevraagde beoefende sport bestaat al.";
                            }
                        } else {
                            echo "Invoer niet geldig. Probeer opnieuw.";
                        }
                    } else {
                        echo "Geen geldige IDs. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['delPractisedSportBut'])) {
                    $unique = $_POST['delPractisedSportBut'];
                    $count = 0;
                    $id = "";
                    $sportCode = "";
                    $contributionYear = "";
                    for ($i = 0; $i < strlen($unique); $i++) {
                        $char = $unique[$i];
                        if ($char == '-') {
                            $count += 1;
                        } else {
                            if ($count == 0) {
                                $id = $id . $char;
                            } else if ($count == 1) {
                                $sportCode = $sportCode . $char;
                            } else if ($count == 2) {
                                $contributionYear = $contributionYear . $char;
                            } else {
                                $id = "";
                                $sportCode = "";
                                $contributionYear = "";
                            }
                        }
                    }
                    if (!validateUniquePractisedSport($db, $id, $sportCode, $contributionYear)) {
                        deletePractisedSport($db, $id, $sportCode, $contributionYear);
                    } else {
                        echo "Geen geldige invoer. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['savePayment'])) {
                    $unique = $_POST['savePayment'];
                    $payment = $_POST['payment'];
                    $count = 0;
                    $id = "";
                    $sportCode = "";
                    $contributionYear = "";
                    for ($i = 0; $i < strlen($unique); $i++) {
                        $char = $unique[$i];
                        if ($char == '-') {
                            $count += 1;
                        } else {
                            if ($count == 0) {
                                $id = $id . $char;
                            } else if ($count == 1) {
                                $sportCode = $sportCode . $char;
                            } else if ($count == 2) {
                                $contributionYear = $contributionYear . $char;
                            } else {
                                $id = "";
                                $sportCode = "";
                                $contributionYear = "";
                            }
                        }
                    }
                    if (!validateUniquePractisedSport($db, $id, $sportCode, $contributionYear) && ($payment == 'ja' || $payment == 'nee')) {
                        updatePayment($db, $id, $sportCode, $contributionYear, $payment);
                    } else {
                        echo "Geen geldige invoer. Probeer opnieuw.";
                    }
                }

                echo "</div>";
                ?>
                <div class="button-new">
                    <button id="new-but" onclick="location.href='new-member.php'">Nieuw</button>
                </div>
            </div>
        </div>
    </div>
</body>